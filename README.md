This repo holds the iOS SmartSkill library as a framework for distributing using the [SpecRepo](https://gitlab.com/enparadigm_public/smartskill_ios_podspec).

1. Add the framework
2. Update the podspec version.
3. push to gitlab.
4. create a tag.
5. Run the below command from proper path on terminal.
`pod repo push smartskill_ios_podspec SmartSkill.podspec --allow-warnings`
