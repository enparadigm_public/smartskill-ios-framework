// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4.2 (swiftlang-1205.0.28.2 clang-1205.0.19.57)
// swift-module-flags: -target armv7-apple-ios9.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Osize -module-name SmartSkill
import AVFoundation
import Alamofire
import AudioToolbox
import Cartography
import CloudKit
import DropDown
import Foundation
import JJFloatingActionButton
import KeychainSwift
import Kingfisher
import Koloda
import Lottie
import MBRadioButton
import SDWebImage
import SQLite3
@_exported import SmartSkill
import Swift
import SwiftSVG
import TTFortuneWheel
import UIKit
import UserNotifications
import YoutubePlayer_in_WKWebView
@_hasMissingDesignatedInitializers public class SmartSkillService {
  public static let sharedInstance: SmartSkill.SmartSkillService
  public var userData: SmartSkill.UserDataModel? {
    get
    set
  }
  public var isSyncing: Swift.Bool {
    get
    set
  }
  public func initiateOTP(mobileNumber: Swift.String, countryCode: Swift.String? = nil, completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func verifyOTP(mobileNumber: Swift.String, otp: Swift.String, countryCode: Swift.String? = nil, completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func getCountries(completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func initiateSecurityKey(userId: Swift.String, completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func verifySecurityKey(userId: Swift.String, securityKey: Swift.String, completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func updateFCM(fcmToken: Swift.String, completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func saveFcmToken(fcmToken: Swift.String)
  public func validateUser(loginId: Swift.String, loginType: SmartSkill.LoginType, completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func clearData()
  public func syncData(completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func changeLanguage(languageId: Swift.Int, languageCode: Swift.String, sourceController: UIKit.UIViewController)
  public func startSmartSkill(sourceController: UIKit.UIViewController, completion: @escaping (Swift.Bool, Any?, SmartSkill.APIError?) -> Swift.Void)
  public func isSmartSkillNotification(userInfo: [Swift.String : Any]) -> Swift.Bool
  public func handleNotification(userInfo: [Swift.String : Any], state: UIKit.UIApplication.State)
  public func openCourse(courseId: Swift.Int, sourceController: UIKit.UIViewController)
  public func loadProfileScreen(sourceController: UIKit.UIViewController)
  public func loadProgressScreen(sourceController: UIKit.UIViewController)
  public func openSubtopic(sourceController: UIKit.UIViewController, subTopicId: Swift.Int, topicId: Swift.Int, courseId: Swift.Int)
  public func loadNotificationSettingsScreen(sourceController: UIKit.UIViewController)
  public func loadPrivacyPolicyScreen()
  public func loadRegisterScreen(sourceController: UIKit.UIViewController, isFromRequestAccess: Swift.Bool? = nil)
  public func videoChallengeConfigData() -> [Swift.String : Any]
  public func addEvents(eventName: Swift.String, properties: [Swift.String : Any])
  public func getKeyChainData(key: Swift.String) -> Swift.String?
  public func canLaunchSDK(jsonData: [Swift.String : Any]) -> Swift.Bool
  public func setUpPluginData(accessToken: Swift.String, refreshToken: Swift.String, uniqueId: Swift.Int, name: Swift.String, languageShortName: Swift.String, userJsonData: [Swift.String : Any]?)
  @objc deinit
}
public enum AppConstant {
  public enum UserDefaults : Swift.String {
    case currentCourse
    case userData
    case metaDataVersion
    case isUserDbSynced
    case shouldShowInAppNotifications
    case notificationSeclectedTime
    case startCourseIntroduction
    case shouldShowQuizFTUE
    case currentLanguage
    case defaultLanguage
    case currentLanguageId
    case currentLanguageName
    case currentLanguageNameInEnglish
    case configData
    case isLiveStramCoachmarkShown
    case name
    case jsonData
    case uniqueID
    case isUserLoggedIn
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
}
extension UIFont {
  public static let loadAllFonts: ()
}
public enum ContentDataType : Swift.Int {
  case subTopic
  case poster
  case video
  case pdf
  case gif
  public static func enumFromContentTypeId(contentTypeId: Swift.Int) -> SmartSkill.ContentDataType?
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public enum NotificationTarget : Swift.String {
  case profile
  case leaderboard
  case course
  case topic
  case subTopic
  case skill
  case sync
  case task
  case survey
  public static func announcementTargetEnumFromString(string: Swift.String) -> SmartSkill.NotificationTarget?
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum LoginType : Swift.Int {
  case mobile
  case email
  case username
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
extension NSObject {
  public class var className: Swift.String {
    get
  }
}
public class SmartSkillConfig {
  public var isAnalyticsEnabled: Swift.Bool
  public var analyticsId: Swift.String?
  public var appDisplayName: Swift.String?
  public var isMultiLanguageEnabled: Swift.Bool
  public var shouldEnableBack: Swift.Bool
  public init(baseUrl: Swift.String)
  public init(baseUrl: Swift.String, analyticsId: Swift.String)
  @objc deinit
}
extension UIColor {
  convenience public init(hex: Swift.String, alpha: CoreGraphics.CGFloat = 1.0)
}
public class UserDataModel {
  final public let userId: Swift.Int
  final public let userName: Swift.String
  final public let appName: Swift.String
  final public let email: Swift.String?
  final public let mobileNumber: Swift.String?
  final public let registeredStatus: Swift.Bool
  final public let registeredDate: Swift.String?
  final public let uniqueId: Swift.String?
  final public let batchId: Swift.Int?
  public init(data: [Swift.String : Any])
  @objc deinit
}
public class SmartSkill {
  public func mainInstance() -> SmartSkill.SmartSkillService?
  public init(config: SmartSkill.SmartSkillConfig)
  @objc deinit
}
@_hasMissingDesignatedInitializers public class APIError {
  final public let title: Swift.String?
  final public let message: Swift.String?
  @objc deinit
}
public enum AnalyticsProperty : Swift.String {
  case name
  case timeSpent
  case postersAvailable
  case pdfsAvailable
  case quizesAvailable
  case videosAvailable
  case postersVisited
  case videosPlayed
  case pdfsVisited
  case quizesFinished
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public enum AnalyticsEvent {
  case launchHomeScreen
  case shareApp
  case viewedPerformance
  case viewedProfile
  case clickHelp
  case clickSync
  case clickNotification
  case clickFAQ
  case syncComplete
  case syncFailed
  case pushNotificationReceived
  case enterAppFromPushNotification
  case enterAppFromBOTDNotification
  case clickedBOTDonHome
  case clickedBOTDNotification
  case timeSpentPerSession
  case startSubtopic
  case finishSubtopic
  case startCourse
  case finishCourse
  case quitSubtopic
  case earnBadge
  case posterShared(name: Swift.String)
  case posterViewed(name: Swift.String)
  case videoShared(name: Swift.String)
  case videoViewed
  case weekOnWeekTrends
  case addiction
  case retention
  case averageTimeSpentOnAppDaily
  case appUsedMostly
  public var eventName: Swift.String {
    get
  }
}
public struct SSTheme {
  public static var primaryColor: UIKit.UIColor
  public static var primaryDarkColor: UIKit.UIColor
  public static var secondaryColor: UIKit.UIColor
  public static var secondaryDarkColor: UIKit.UIColor
  public static var accentColor: UIKit.UIColor
  public static var accentDarkColor: UIKit.UIColor
  public static var disableColor: UIKit.UIColor
  public static var buttonTextColor: UIKit.UIColor
}
extension UITextField {
  @objc override dynamic open func canPerformAction(_ action: ObjectiveC.Selector, withSender sender: Any?) -> Swift.Bool
}
extension UITextView {
  @objc override dynamic open func canPerformAction(_ action: ObjectiveC.Selector, withSender sender: Any?) -> Swift.Bool
}
extension SmartSkill.AppConstant.UserDefaults : Swift.Equatable {}
extension SmartSkill.AppConstant.UserDefaults : Swift.Hashable {}
extension SmartSkill.AppConstant.UserDefaults : Swift.RawRepresentable {}
extension SmartSkill.ContentDataType : Swift.Equatable {}
extension SmartSkill.ContentDataType : Swift.Hashable {}
extension SmartSkill.ContentDataType : Swift.RawRepresentable {}
extension SmartSkill.NotificationTarget : Swift.Equatable {}
extension SmartSkill.NotificationTarget : Swift.Hashable {}
extension SmartSkill.NotificationTarget : Swift.RawRepresentable {}
extension SmartSkill.LoginType : Swift.Equatable {}
extension SmartSkill.LoginType : Swift.Hashable {}
extension SmartSkill.LoginType : Swift.RawRepresentable {}
extension SmartSkill.AnalyticsProperty : Swift.Equatable {}
extension SmartSkill.AnalyticsProperty : Swift.Hashable {}
extension SmartSkill.AnalyticsProperty : Swift.RawRepresentable {}
